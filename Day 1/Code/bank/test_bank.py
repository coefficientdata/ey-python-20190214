import pytest

from bank.bank import BankAccount


def test_bank_account():
    """If the starting balance for a BankAccount is either not a number, or negative,
       we should see an exception.
       """
    # Throws error if starting balance is not a number?
    with pytest.raises(TypeError):
        BankAccount(balance="pot of gold")
    # Expected behaviour with positive balance, should run fine without error
    BankAccount(balance=10)
    # Negative balance should throw an exception
    with pytest.raises(ValueError):
        BankAccount(balance=-10)


def test_deposit():
    """deposit() method should add money to the BankAccount balance, and return the current balance."""
    my_account = BankAccount()
    assert my_account.deposit(10) == 10
    assert my_account.deposit(10) == 20
    assert my_account.deposit("100") == 120
    with pytest.warns(UserWarning):
        my_account.deposit("peanuts")
    assert my_account.balance == 120


def test_withdraw():
    """withdraw() method should remove money from the BankAccount balance, and return the current balance.
       It should not allow more money to be removed than is currently in the account."""
    my_account = BankAccount(account_name="John Sandall", balance=0)
    assert my_account.deposit(100) == 100
    assert my_account.withdraw(10) == 90
    with pytest.raises(Exception):
        my_account.withdraw(1000)
    assert my_account.withdraw("90") == 0
    with pytest.warns(UserWarning):
        my_account.withdraw("all the money")
    assert my_account.balance == 0
