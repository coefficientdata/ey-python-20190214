import warnings


def add(x, y):
    return x + y


class BankAccount:

    def __init__(self, account_name=None, balance=0):
        if type(balance) not in [int, float]:
            raise TypeError('Starting balance must be of type `int` or `float`.')
        if balance < 0:
            raise ValueError('Starting balance cannot be less than zero.')
        self.account_name = account_name
        self.balance = balance

    def __repr__(self):
        return "BankAccount (balance: £{})".format(self.balance)

    def deposit(self, amount):
        try:
            amount = float(amount)
        except ValueError as e:
            warnings.warn("Warning: {}".format(e))
            pass
        else:
            self.balance += amount
            return self.balance

    def withdraw(self, amount):
        try:
            amount = float(amount)
        except ValueError as e:
            warnings.warn("Warning: {}".format(e))
            pass
        else:
            if amount > self.balance:
                raise ValueError("Amount specified exceeds account balance! \
                                  Current balance: £{}".format(self.balance))
            # No need for an else here, it's implied
            self.balance -= amount
            return self.balance
